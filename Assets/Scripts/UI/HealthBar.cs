﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class HealthBar : MonoBehaviour
    {
        public Image HpFill;
        public Image DeltaFill;
        public float DrainSpeed;

        private int maxHp;

        public void Initialize(int initialHp)
        {
            maxHp = initialHp;
            HpFill.fillAmount = 1;
        }

        public void UpdateBar(int currentHp) => 
            HpFill.fillAmount = (float) currentHp / maxHp;

        void Update() => 
            DeltaFill.fillAmount = 
                Mathf.Lerp(DeltaFill.fillAmount, HpFill.fillAmount, DrainSpeed * Time.deltaTime);
    }
}
