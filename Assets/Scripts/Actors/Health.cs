﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Actors
{
    public class Health : MonoBehaviour
    {
        public float HitProtectionTime;

        int startingHp;
        private Action onDeath;
        private Action<int> onHurt;
        private bool canGetDamage = true;
        public int CurrentHp { get; private set; }

        public void Initialize(int hitPoints, Action<int> onHurt, Action onDeath)
        {
            this.onDeath = onDeath;
            this.onHurt = onHurt;
            startingHp = hitPoints;
            CurrentHp = startingHp;
        }

        public void ReceiveDamage(int damage)
        {
            if (canGetDamage)
            {
                CurrentHp -= damage;
                if (CurrentHp <= 0)
                    onDeath();
                else
                    onHurt(damage);
            }

            StartCoroutine(ProtectCharacter());
        }

        IEnumerator ProtectCharacter()
        {
            canGetDamage = false;
            yield return new WaitForSeconds(HitProtectionTime);
            canGetDamage = true;
        }
    }
}
