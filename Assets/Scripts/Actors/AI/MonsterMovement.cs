﻿using UnityEngine;

namespace Assets.Scripts.Actors.AI
{
    public class MonsterMovement : MonoBehaviour
    {
        public CollisionDetection Detector;

        private MonsterView view;
        float speed;

        public void MoveTowards(Vector3 target)
        {
            var moveVector = (target - transform.position).normalized;
            moveVector *= speed * Time.deltaTime;

            if (Detector.CanMoveTowards(moveVector))
                transform.position += moveVector;

            view.ShowMovement(moveVector);
        }

        public void Initialize(float speed, MonsterView view)
        {
            this.view = view;
            this.speed = speed;
        }

        public void Stop() => 
            view.StopMoving();
    }
}
