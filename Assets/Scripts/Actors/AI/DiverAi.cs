﻿using System.Collections;
using Assets.Scripts.Actors.PlayingCharacter;
using Assets.Scripts.Directors;
using UnityEngine;

namespace Assets.Scripts.Actors.AI
{
    public class DiverAi : BaseAi
    {
        public float ProximityThreshold;
        public float AttackFrequency;
        public float FrequencyModifier;
        public DiveAttack DiveAttack;
        public CollisionDetection Detector;
        public float HoverOffsetDistance;

        private float attackInterval => AttackFrequency + Random.Range(-FrequencyModifier, FrequencyModifier);
        private Vector3 targetPositionOffset;

        private bool IsFarFromHero =>
            Vector3.Distance(transform.position, hero.Position) > ProximityThreshold;

        public override void Initialize(ActorProvider actorProvider, ActorStats stats, MonsterMovement movement,
            MonsterView view)
        {
            base.Initialize(actorProvider, stats, movement, view);
            DiveAttack.Initialize(Detector, Cone, view);
            GenerateRandomOffset();
        }

        void Update()
        {
            if (!isPerformingAction)
            {
                if (IsFarFromHero)
                    movement.MoveTowards(hero.Position + targetPositionOffset);
                else
                    Dive();
            }

            if(DiveAttack.enabled)
                OnSwing();
        }

        private void Dive()
        {
            DiveAttack.Do(hero.Position - transform.position);
            Cone.LookAt(hero.Position);
            isPerformingAction = true;
            view.ShowAttack();
            StartCoroutine(WaitForNextAttack());
        }

        private IEnumerator WaitForNextAttack()
        {
            GenerateRandomOffset();
            yield return new WaitForSeconds(attackInterval);
            isPerformingAction = false;
        }

        private void GenerateRandomOffset() => targetPositionOffset =
            new Vector3(Random.Range(-HoverOffsetDistance, HoverOffsetDistance), 0f,
                Random.Range(-HoverOffsetDistance, HoverOffsetDistance));
    }
}
