﻿using Assets.Scripts.Directors;
using UnityEngine;

namespace Assets.Scripts.Actors.AI
{
    public class Monster : MonoBehaviour
    {
        public ActorStats Stats;

        public MonsterMovement Movement;
        public MonsterView View;
        public BaseAi Brain;
        public Health Health;
        public ProximityActivator Activator;

        public void Initialize(ActorProvider actorProvider)
        {
            View.Initialize(Stats.PainTolerance);
            Movement.Initialize(Stats.WalkSpeed, View);
            Brain.Initialize(actorProvider, Stats, Movement, View);
            Health.Initialize(Stats.HitPoints, View.ShowHurt, Disable);
            Activator.Initialize(actorProvider, Brain, View);
        }

        private void Disable()
        {
            enabled = false;
            Brain.Disable();
            View.ShowDeath();
        }

        public void ReceiveDamage(int damage)
        {
            if (enabled)
                Health.ReceiveDamage(damage);
        }
    }
}
