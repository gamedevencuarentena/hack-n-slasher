﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Actors.Status
{
    public class StatusDisplay : MonoBehaviour
    {
        public Text StatusText;
        public Animator Animator;

        public void ShowDamage(int damage)
        {
            StatusText.text = damage.ToString();
            Animator.SetTrigger("ShowDamage");
        }
    }
}
