﻿using System;

namespace Assets.Scripts.Actors
{
    [Serializable]
    public class ActorStats
    {
        public int HitPoints;
        public float WalkSpeed;
        public int MinDamage;
        public int MaxDamage;
        public int PainTolerance;
    }
}
