﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Actors
{
    public class FloorDetector: MonoBehaviour
    {
        public float MidHeight;
        public LayerMask FloorLayer;
        public float FallSpeed;

        void Update()
        {
            var ray = new Ray(transform.position + new Vector3(0f, MidHeight, 0f), Vector3.down);
            var hit = new RaycastHit();

            if (Physics.Raycast(ray, out hit, MidHeight * 1.1f, FloorLayer))
            {
                var pos = transform.position;
                pos.y = hit.point.y;
                transform.position = pos;
            }
            else if(transform.position.y > -100f)
                transform.position += Vector3.down * FallSpeed * Time.deltaTime;
        }
    }
}
