﻿using Assets.Scripts.Actions;
using Assets.Scripts.Directors;
using Assets.Scripts.Player;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Actors.PlayingCharacter
{
    public class Character : MonoBehaviour
    {
        public ActorStats Stats;
        public CharacterMovement Movement;
        public CharacterActions Actions;
        public CharacterView View;
        public Dash Dash;
        public CollisionDetection CollisionDetector;
        public Health Health;

        private PlayerInput input;
        private HealthBar healthBar;

        public Vector3 Position => transform.position;

        public void Initialize(PlayerInput input, HealthBar healthBar, ActorProvider actorProvider)
        {
            this.input = input;
            this.healthBar = healthBar;

            Movement.Initialize(Stats.WalkSpeed, CollisionDetector, View);
            Actions.Initialize(Stats, Movement, Dash, View, actorProvider);
            Dash.Initialize(Movement, CollisionDetector, View);
            Health.Initialize(Stats.HitPoints, View.ShowHurt, OnDeath);
            input.Initialize(Movement, Actions);
            healthBar.Initialize(Stats.HitPoints);
        }

        private void OnDeath()
        {
            enabled = false;
            Dash.ForceStop();
            input.Disable();
            View.ShowDeath();
        }

        public void ReceiveDamage(int damage)
        {
            if (enabled)
            {
                Health.ReceiveDamage(damage);
                healthBar.UpdateBar(Health.CurrentHp);
            }

        }
    }
}
