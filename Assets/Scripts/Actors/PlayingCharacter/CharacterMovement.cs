﻿using UnityEngine;

namespace Assets.Scripts.Actors.PlayingCharacter
{
    public class CharacterMovement : MonoBehaviour
    {
        float speed;
        CharacterView view;
        private CollisionDetection collisionDetector;

        public void Initialize(float speed, CollisionDetection collisionDetector, CharacterView view)
        {
            this.speed = speed;
            this.collisionDetector = collisionDetector;
            this.view = view;
        }

        public void Move(Vector2 inputVector)
        {
            var movementVector = new Vector3(inputVector.x, 0f, inputVector.y) * speed * Time.deltaTime;

            if (enabled && collisionDetector.CanMoveTowards(movementVector))
            {
                transform.position += movementVector;
                view.ShowMovement(inputVector);
                Debug.DrawLine(transform.position, transform.position + movementVector.normalized, Color.blue);
            }
        }

        public void Disable() => enabled = false;

        public void Enable() => enabled = true;
    }
}