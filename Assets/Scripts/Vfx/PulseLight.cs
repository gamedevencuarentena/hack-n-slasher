﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Vfx
{
    public class PulseLight : MonoBehaviour
    {
        public Light Light;
        public float Frequency;
        public float DeltaIntensity;

        private float initialIntensity;

        void Start()
        {
            initialIntensity = Light.intensity;
            StartCoroutine(WaitAndPulse());
        }

        private IEnumerator WaitAndPulse()
        {
            yield return new WaitForSeconds(Frequency);
            Light.intensity = Random.Range(initialIntensity - DeltaIntensity,
                initialIntensity + DeltaIntensity);
            StartCoroutine(WaitAndPulse());
        }
    }
}
