﻿using UnityEngine;

namespace Assets.Scripts.Actions
{
    public class ConeArea : ShapedArea
    {
        public float Range;
        public float Angle;
        
        // cos(theta) = 1/hyp
        // sin(theta) = a/hyp
        // a = hyp * sin(theta)
        // hyp = 1/cos(theta)
        // a = sin(theta)/cos(theta)

        float Aperture => Mathf.Sin(Angle/2 * Mathf.Deg2Rad) 
                          / Mathf.Cos(Angle/2 * Mathf.Deg2Rad);

        void OnDrawGizmos()
        {
            var arcLimitRight = new Vector3(Aperture, 0, 1) * Range;
            var arcLimitLeft = new Vector3(-Aperture, 0, 1) * Range;

            Debug.DrawLine(transform.position, transform.TransformPoint(arcLimitLeft), Color.yellow);
            Debug.DrawLine(transform.position, transform.TransformPoint(arcLimitRight), Color.yellow);
        }
        
        public bool IsInArea(Vector3 target)
        {
            var targetRelative = transform.InverseTransformPoint(target);
            var inRange = Vector3.Distance(transform.position, target) < Range;
            var inArcRight = targetRelative.x + Aperture * targetRelative.z > 0;
            var inArcLeft = targetRelative.x - Aperture * targetRelative.z < 0;
            
            var isInArc = inArcRight & inArcLeft;
            return inRange && isInArc;
        }
    }
}
